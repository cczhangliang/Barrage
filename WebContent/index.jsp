<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%  
    String path = request.getContextPath();  
    String socPath="ws://"+request.getServerName()+":"+request.getServerPort()+path+"/";  
	%>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<title>弹幕实现</title>
</head>
<body>
	<div id="show">
	
	</div>
	<input type="text" id="writeMsg"/>   
	<input type="button" value="sendSmgToServer" onclick="sendMsg()"/> 
</body>
<script type="text/javascript">
	var wsuri = "<%=socPath%>socket";
	var ws = null ;
	var index = 0;
	init();
	function init(){
		startWebSocket();
	}
	function startWebSocket(){
		 if ('WebSocket' in window)  
             ws = new WebSocket(wsuri);  
         else if ('MozWebSocket' in window)  
             ws = new MozWebSocket(wsuri);  
         else  
             console.error("not support WebSocket!");  
		 
		 ws.onmessage = function(evt) { 
			 var message = evt.data;
             $("#show").append("<div id='"+index+"'>"+message+"</div>");
             //飞起来
             launch();
             console.info(evt);  
         };  

         ws.onclose = function(evt) {  
               
             alert("close");  
             console.info(evt);  
         };  

         ws.onopen = function(evt) {  
             alert("open");  
             console.info(evt);  
         }; 
	}
	//发送消息
	function sendMsg(){           
        ws.send($("#writeMsg").val());
        //清空
        $("#writeMsg").val("");
    }  
	//动画效果
	function launch(){
		$("#"+index).animate({
			left:"800px",
		},800,function(){
			index++;
		});
	}
</script>
</html>